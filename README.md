### Requirements:

1. Write a simple application in Spring Boot that provides movie suggestions for a user based on their movie genre preference. You can choose other tools and libraries that you use yourself. Provide a REST API for this. We want to use multiple external services like Netflix or IMDB for this. Let's assume that you don't have access to those external services yet, and have to solve the problem so that you get the access later. What kind of architecture would you use to achieve this? How would you write tests for the app and design it for testability?
2. Just as a clarification, persistence is not required in the scope of the case.

# Getting Started
Here you find application with following technologies Springfox-swagger2, Spock tests with Groovy, JPA.
There is multilayer server architecture, I used DTO pattern with Swagger API. 

It is a baseline application which matches requirements mentioned above. As an extension I can assume:

1. More detailed API, but it depend on what data will be requested from outbound services. Present API was formed according to guidelines. It was selected base domains within mentioned in requirments. It provides easy upgrade into the future.
2. Usage of AsyncTaskExecutor for services for improvement of velocity and utility of multi core processors
3. From points of robustness and load distribution it could be good to have cache and pagination

### RUNNING THE APPLICATION:

* This is standard Spring-boot application.
 you can start it  java -jar target/spring-boot-rating-service-0.0.1-SNAPSHOT.jar
* Then you can see documentation, model and [test API via the UI HERE ](http://localhost:8080/swagger-ui.html#/)
* Run test with mvn clean test. As far as it is very small application I didn’t separate unit and integration test.
* There are several comments of mine scattered into code


