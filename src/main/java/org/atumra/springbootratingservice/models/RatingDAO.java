package org.atumra.springbootratingservice.models;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *  This is example of JPA realization. As far as Swagger has limitation regarding the properties which are
 *  shown with REST UI interface and returns with response. Authors recommends to use different model for
 *  response and request. In our case it is overhead so I decided to use an additional entity for JPA.
 *  In such way we split logic of interaction with DAO within business layer and data layer,
 *  and DTO into presentation layer.
 */

@Entity
@Table(name = "ratings")
public class RatingDAO {

    @Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "genre", nullable = false)
    private String genre;

    @Column(name = "amount", nullable = false)
    private int amount;

    @Column(name = "response", nullable = false)
    @ElementCollection(targetClass=String.class)
    private List<String> response = new ArrayList<>();

    public RatingDAO(String genre, int amount) {
		this.genre = genre;
		this.amount = amount;
    }
	public RatingDAO(String genre, int amount, List<String> response) {
		this.genre = genre;
		this.amount = amount;
		this.response = response;
	}

    /**
     * @return the amount
     */

    public int getAmount() {
        return amount;
    }

    /**
     * @param amount to set
     */
    public void setAmount(String amount) {
		this.genre = amount;
	}

    /**
     * @return the genre
     */

    public String getGenre() {
        return genre;
    }

     /**
     * @param genre to set
     */
    public void setGenre(String genre) {
		this.genre = genre;
	}

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id to set
     */
    public void setId(long id) {
		this.id = id;
	}

     /**
     * @param response the response to set
     */
    public void setResponse(List<String> response) {
        this.response = response;
    }

	 /**
     * @return the response
     */
    public List<String> getResponse() {
        return response;
    }

    @Override
	public String toString() {
		return "Rating [id=" + id + " genre=" + genre + ", requested amount=" + amount + ", response=" + Arrays.toString(response.toArray())
				+ "]";
	}

}