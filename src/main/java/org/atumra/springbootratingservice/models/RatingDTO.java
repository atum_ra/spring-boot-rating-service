package org.atumra.springbootratingservice.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description="Base class of rating request")
public class RatingDTO {

	@ApiModelProperty(notes = "Requested genre of films")
	private final String genre;

	@ApiModelProperty(notes = "Requested amount of films in response")
	private final int amount;

	@ApiModelProperty(notes = "Response with list of films", allowEmptyValue=true)
	private List<String> response = new ArrayList<>();

	public RatingDTO(String genre, int amount) {
		this.genre = genre;
		this.amount = amount;
    }
	public RatingDTO(String genre, int amount, List<String> response) {
		this.genre = genre;
		this.amount = amount;
		this.response = response;
	}

	public String getGenre() {
		return genre;
	}

	public int getAmount() {
		return amount;
    }

    /**
     * @param response the response to set
     */
    public void setResponse(List<String> response) {
        this.response = response;
    }

	 /**
     * @return the response
     */

    public List<String> getResponse() {
        return response;
    }

	@Override
	public String toString() {
		return "Rating [genre=" + genre + ", requested amount=" + amount + ", response=" + Arrays.toString(response.toArray())
				+ "]";
	}

}
