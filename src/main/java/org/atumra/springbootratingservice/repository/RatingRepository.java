package org.atumra.springbootratingservice.repository;

import org.atumra.springbootratingservice.models.RatingDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RatingRepository extends JpaRepository<RatingDAO, Long>{

}