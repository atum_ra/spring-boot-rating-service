package org.atumra.springbootratingservice.service;

import org.atumra.springbootratingservice.models.RatingDTO;

public interface OutboundRequestInterface {

    RatingDTO request(RatingDTO ratingDTO);
}