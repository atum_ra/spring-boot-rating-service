package org.atumra.springbootratingservice.service;

import java.util.ArrayList;
import java.util.List;

import org.atumra.springbootratingservice.models.RatingDTO;
import org.springframework.stereotype.Service;

@Service
public class OutboundRequestIMDB implements OutboundRequestInterface {

    @Override
    public RatingDTO request(RatingDTO ratingDTO) {
        List<String> response = new ArrayList<>();
        response.add("IMDB film with " + ratingDTO.getGenre());
        ratingDTO.setResponse(response);
        return ratingDTO;
    }

}