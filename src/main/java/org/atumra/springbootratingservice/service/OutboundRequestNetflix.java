package org.atumra.springbootratingservice.service;

import java.util.ArrayList;
import java.util.List;

import org.atumra.springbootratingservice.models.RatingDTO;
import org.springframework.stereotype.Service;

@Service
public class OutboundRequestNetflix implements OutboundRequestInterface {

    @Override
    public RatingDTO request(RatingDTO ratingDTO) {
        List<String> response = new ArrayList<>();
        response.add("Netflix film with " + ratingDTO.getGenre());
        response.add("films with unique logic of request");
        ratingDTO.setResponse(response);
        return ratingDTO;
    }

}