package org.atumra.springbootratingservice.controller;

import java.util.logging.Logger;

import org.atumra.springbootratingservice.exception.ResourceNotFoundException;
import org.atumra.springbootratingservice.models.RatingDTO;
import org.atumra.springbootratingservice.service.OutboundRequestIMDB;
import org.atumra.springbootratingservice.service.OutboundRequestNetflix;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/v1")
@Api(value = "Rating films service REST API")
public class SwaggerController {
	// @Autowired
	// private RatingRepository ratingRepository;

	@Autowired
	OutboundRequestNetflix outboundRequestNetflix;
	@Autowired
	OutboundRequestIMDB outboundRequestIMDB;

	private final static Logger LOGGER = Logger.getLogger("REST-CONTROLLER");

	@ApiOperation(value = "Get an list of films from Netflix", response = RatingDTO.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list"),
		@ApiResponse(code = 400, message = "Invalid input provided"),
		@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
		@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
		@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@GetMapping("/Netflix")
	public ResponseEntity<RatingDTO> getRatingNetflix(
			@ApiParam(name = "genre", value = "requesting genre", required = true) @RequestParam(name = "genre") String genre,
			@ApiParam(name = "amount", value = "requesting amount of films") @RequestParam(name = "amount", defaultValue = "10") int amount)
			throws ResourceNotFoundException {
		LOGGER.info("Processing request on Netflix with genre " + genre);
		if (!genre.matches("[a-zA-Z]+")) throw new ResourceNotFoundException("Genre is a string type");
        RatingDTO ratingDTO = new RatingDTO(genre, amount);
		ratingDTO = outboundRequestNetflix.request(ratingDTO);
		return ResponseEntity.ok().body(ratingDTO);
	}

	@ApiOperation(value = "Get an list of films from IMDB", response = RatingDTO.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list"),
		@ApiResponse(code = 400, message = "Invalid input provided"),
		@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
		@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
		@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@GetMapping("/IMDB")
	public ResponseEntity<RatingDTO> getRatingIMDB(
			@ApiParam(name = "genre", value = "requesting genre", required = true) @RequestParam(name = "genre") String genre,
			@ApiParam(name = "amount", value = "requesting amount of films") @RequestParam(name = "amount", defaultValue = "10") Integer amount) {
		LOGGER.info("Processing request on IMDB with genre " + genre);
        RatingDTO ratingDTO = new RatingDTO(genre, amount);
		ratingDTO = outboundRequestIMDB.request(ratingDTO);
		return ResponseEntity.ok().body(ratingDTO);
	}

}