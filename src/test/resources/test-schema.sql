DROP TABLE IF EXISTS `ratings`;
CREATE TABLE `ratings`
(
    `id`         bigint(20) NOT NULL AUTO_INCREMENT,
    `genre`      varchar(255) NOT NULL,
    `amount`     smallint(3) NOT NULL,
    `response`   varchar(5000) DEFAULT NULL,

    PRIMARY KEY (`id`)
);