package org.atumra.springbootratingservice

import spock.lang.Specification
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationContext

//For this integration test, we need to start up the ApplicationContext, which is what @SpringBootTest does for us.
@SpringBootTest(classes = DemoApplication)

class DemoApplicationSpec extends Specification {

    @Autowired
    ApplicationContext context

    def "test context loads"() {
    expect:
    context != null
    context.containsBean("swaggerController")
    context.containsBean("outboundRequestIMDB")
    context.containsBean("outboundRequestNetflix")

}}