package org.atumra.springbootratingservice.service

import org.atumra.springbootratingservice.models.RatingDTO
import org.atumra.springbootratingservice.service.OutboundRequestIMDB
import org.atumra.springbootratingservice.service.OutboundRequestNetflix
import org.atumra.springbootratingservice.service.OutboundRequestInterface
import org.atumra.springbootratingservice.repository.RatingRepository

import spock.lang.Specification
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class OutboundRequestSpec extends Specification {

    def ratingDTO = new RatingDTO('genre', 10)

    @Autowired
    @Qualifier("outboundRequestIMDB") 
    OutboundRequestInterface outboundRequestIMDB;

    void setup() {
        assert ratingDTO != null
    }

    def "OutboundRequestInterface instatntiated as OutboundRequestIMDB should return response property"() {
        given:
        def responseDTO = outboundRequestIMDB.request(ratingDTO)

        expect:
        with(responseDTO) {
           response == ['IMDB film with genre']
        }
    }

}
