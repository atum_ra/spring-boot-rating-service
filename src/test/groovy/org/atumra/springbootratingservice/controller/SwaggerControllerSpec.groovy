package org.atumra.springbootratingservice.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import spock.lang.Narrative
import spock.lang.Specification
import spock.lang.Title
import org.spockframework.spring.SpringBean

@Title("SwaggerController Spec")
@Narrative("The Specification of the behaviour of the SwaggerController")
@AutoConfigureMockMvc(secure=false)
@WebMvcTest()
class SwaggerConrollerSpec extends Specification {

    @Autowired
    private MockMvc mvc

    def "when get is performed for Netflix then the response has status 200"() {
        expect: "Status is 200"
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/Netflix?genre=horror"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andReturn()
            .response.contentAsString == "{\"genre\":\"horror\",\"amount\":10,\"response\":[\"Netflix film with horror\",\"films with unique logic of request\"]}"
    }

    def "when get is performed for IMDB then the response has status 200"() {
        expect: "Status is 200"
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/IMDB?genre=thriller"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andReturn()
            .response.contentAsString == "{\"genre\":\"thriller\",\"amount\":10,\"response\":[\"IMDB film with thriller\"]}"
    }

    def "when get is performed for IMDB then the response has status 200"() {
        expect: "Genre is thriller and amount is 5"
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/IMDB?genre=thriller&amount=5"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andReturn()
            .response.contentAsString == "{\"genre\":\"thriller\",\"amount\":5,\"response\":[\"IMDB film with thriller\"]}"
    }

    def "when get is performed for Netflix then the response has status 400"() {
        expect: "Genre is 10"
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/Netflix?genre=10"))
            .andExpect(MockMvcResultMatchers.status().is(404));
    }

}