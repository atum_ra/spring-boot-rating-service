package org.atumra.springbootratingservice.repository

import org.atumra.springbootratingservice.models.RatingDAO
import org.atumra.springbootratingservice.repository.RatingRepository

import spock.lang.Specification
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest


//@DataJpaTest
// context mentioned above does not work properly with springfox-swagger2
// there is an ticket at github regarding this
// https://stackoverflow.com/questions/55015539/test-with-datajpatest-given-no-bean-of-type-servletcontext

@SpringBootTest
class RatingRepositorySpec extends Specification {

    @Autowired
    RatingRepository ratingRepo

    void setup() {
        assert ratingRepo != null
    }

    def "can save an rating"() {
        given:
        def genre = 'comedy'
        def amount = 5
        RatingDAO rating = ratingRepo.save(new RatingDAO(genre, amount))

        expect:
        with(rating) {
            id == 1
            genre == 'comedy'
            amount == 5
        }
    }

     void cleanup() {
        ratingRepo = null
    }

}
