package org.atumra.springbootratingservice.models

import spock.lang.Specification

class RatingDTOSpec extends Specification {

    def "constructor sets attributes to default values"() {
        given:
        RatingDTO ratingDTO = new RatingDTO("fiction", 2)
        assert ratingDTO != null

        expect:
        with(ratingDTO) {
            genre == "fiction"
            amount == 2
            response != null
            response.getClass() == ArrayList
        }

    }

}
